let http = require("http");



//mock data
let users = [

	{
		username: "moonknight1999",
		email: "moonGod@gmail.com",
		password: "moonKnightStrong"
	},
	{
		username: "kitkatMachine",
		email: "notSponsored@gmail.com",
		password: "kitkatForever"
	}

];

let courses = [

	{
		name: "Science 101",
		price: 2500,
		isActive: true
	},
	{
		name: "English 101",
		price: 2500,
		isActive: true
	}
];




http.createServer((req, res) => {

	console.log(req.url);
	console.log(req.method);

	if (req.url === "/" && req.method === "GET") {

		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Hello world. this rout checks for GET Method")

	} else if (req.url === "/" && req.method === "POST") {

		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end("Hello world. this route checks for POST Method")

	} else if (req.url === "/" && req.method === "PUT") {

		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end("Hello world. this route checks for PUT Method")
		
	} else if (req.url === "/" && req.method === "DELETE") {

		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end("Hello world. this route checks for DELETE Method")
		
	}  else if (req.url === "/users" && req.method === "GET") {

		res.writeHead(200, {'Content-Type': 'application/json'})
		res.end(JSON.stringify(users))
		
	} else if (req.url === "/users" && req.method === "POST") {

		let requestBody = "";

		req.on('data', function(data) {
	
			requestBody += data;

		})

		req.on('end', function() {

			requestBody = JSON.parse(requestBody);

			let newUser = {
				username: requestBody.username,
				email: requestBody.email,
				password: requestBody.password
			}

			users.push(newUser);
			
			res.writeHead(200, {'Content-Type': 'application/json'})
			res.end(JSON.stringify(users));
		
		})
		
	}   else if (req.url === "/courses" && req.method === "GET") {

		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(courses));
		
	} else if (req.url === "/courses" && req.method === "POST") {

		let requestBody = "";

		req.on('data', function(data) {

			requestBody += data;

		});

		req.on('end', function() {

			requestBody = JSON.parse(requestBody);

			let  newCourse = {
				name: requestBody.name,
				price: requestBody.price,
				isActive: requestBody.isActive
			}

			courses.push(newCourse);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(JSON.stringify(courses));
		});

	}



}).listen(4000);

console.log("runnning on 4000");

/*
	Common HTTP METHODS"

		GET - indicates that the client request wants to retrieve data. 

		POST - indicates that the clients request wants to post data and create a document

		PUT - indicates that client request wants to input data and update document.

		DELETE - indicates client request wants to delete a document.
*/